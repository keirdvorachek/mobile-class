﻿using Xamarin.Forms;

namespace AgeCalculator
{
    public partial class AgeCalculatorPage : ContentPage
    {



        public AgeCalculatorPage()
        {
            InitializeComponent();


        }


        void Guy_Clicked(object sender, System.EventArgs e)
        {
            int intAge;
            int intOutputAge;
            int.TryParse(txtAge.Text, out intAge);

            intOutputAge = intAge / 2 + 7;
            lblOutput.Text = intOutputAge.ToString();
        }

        void Lady_Clicked(object sender, System.EventArgs e)
        {
            int intAge;
            int intOutputAge;
            int.TryParse(txtAge.Text, out intAge);

            intOutputAge = (intAge - 7) * 2;
            lblOutput.Text = intOutputAge.ToString();
        }
    }
}
