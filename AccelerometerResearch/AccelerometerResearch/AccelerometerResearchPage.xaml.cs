﻿using Xamarin.Forms;
using Xamarin.Essentials;

namespace AccelerometerResearch
{
    public partial class AccelerometerResearchPage : ContentPage
    {
        public AccelerometerResearchPage()
        {
            InitializeComponent();
        }
        void Start_Clicked(object sender, System.EventArgs e)
        {
            if (Accelerometer.IsMonitoring)
                return;

            Accelerometer.ReadingChanged += Accelerometer_ReadingChanged;

            Accelerometer.Start(SensorSpeed.UI);
        }

        void Stop_Clicked(object sender, System.EventArgs e)
        {
            if (!Accelerometer.IsMonitoring)
                return;

            Accelerometer.ReadingChanged -= Accelerometer_ReadingChanged;

            Accelerometer.Stop();
        }

        void Accelerometer_ReadingChanged(object sender, AccelerometerChangedEventArgs e)
        {
            lblXAxis.Text = e.Reading.Acceleration.X.ToString();
            lblYAxis.Text = e.Reading.Acceleration.Y.ToString();
            lblXAxis.Text = e.Reading.Acceleration.Z.ToString();
        }
    }
}
