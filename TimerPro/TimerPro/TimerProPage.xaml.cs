﻿using Xamarin.Forms;
using TimerPro.Custom;
using System;

namespace TimerPro
{
    public partial class TimerProPage : ContentPage
    {

        TimerLogic oTimerLogic;
        bool boolIsRunning;

        public TimerProPage()
        {
            InitializeComponent();
            oTimerLogic = new TimerLogic();
        }

        void Start_Clicked(object sender, System.EventArgs e)
        {
            btnStop.IsEnabled = true;
            btnStart.IsEnabled = false;
            boolIsRunning = true;

            Device.StartTimer(TimeSpan.FromSeconds(1),() => {
                if(boolIsRunning)
                {
                    oTimerLogic.SetTickCount();
                    lblDisplay.Text = oTimerLogic.GetFormattedString();    
                }

                return boolIsRunning;
            });
        }

        void Stop_Clicked(object sender, System.EventArgs e)
        {
            btnStart.IsEnabled = true;
            btnStop.IsEnabled = false;
            boolIsRunning = false;
        }

        void Reset_Clicked(object sender, System.EventArgs e)
        {
            oTimerLogic.Reset();
            lblDisplay.Text = oTimerLogic.GetFormattedString();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            btnStart.IsEnabled = true;
            btnStop.IsEnabled = false;
            lblDisplay.Text = "00:00:00";
        }
    }
}
