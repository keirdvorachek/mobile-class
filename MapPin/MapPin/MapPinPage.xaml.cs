﻿using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Collections.Generic;
using MapPin.Custom;

namespace MapPin
{
    public partial class MapPinPage : ContentPage
    {
        public MapPinPage()
        {
            InitializeComponent();
            CustomPin pin = new CustomPin
            {
                Type = PinType.Place,
                Position = new Position(37.79752, -122.40183),
                Label = "Xamarin San Francisco Office",
                Address = "394 Pacific Ave, San Francisco CA",
                Name = "Xamarin",
                Url = "htt[://xamarin.com/about/"
            };
            customMap.CustomPins = new List<CustomPin> { pin };
            customMap.Pins.Add(pin);
            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(37.79752, -122.40183), Distance.FromMiles(1.0)));
        }
    }
}
