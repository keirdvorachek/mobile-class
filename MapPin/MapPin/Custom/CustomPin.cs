﻿using System;
using Xamarin.Forms.Maps;

namespace MapPin.Custom
{
    public class CustomPin : Pin
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
