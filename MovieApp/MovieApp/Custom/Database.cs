﻿using System;
using MovieApp.Models;
using SQLite.Net;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MovieApp.Custom
{
    public class Database
    {
        SQLiteConnection database;
        static object locker = new object();

        public Database()
        {
            //Get Connection String
            database = DependencyService.Get<ISQLite>().GetConnection();

            //Create Database
            database.CreateTable<Movie>();
        }

        public void SaveMovie(Movie movie)
        {
            lock(locker)
            {
                database.Insert(movie); 
            }

        }

        public List<Movie> GetMovies()
        {
            var Result = database.Query<Movie>("SELECT * FROM [Movie]");
            //var Result = (from db in database.Table<Movie>() select db);
            return Result;
        }
    }
}
