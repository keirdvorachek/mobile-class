﻿using System;
using System.IO;
using SQLite.Net;
using SQLite.Net.Platform.XamarinIOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(Inventory.iOS.Custom.SQLite_iOS))]

namespace Inventory.iOS.Custom
{
    public class SQLite_iOS
    {
        public SQLite_iOS()
        {
        }

        public SQLiteConnection GetConnection()
        {
            var documentPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var libraryPath = Path.Combine(documentPath, "..", "Library");
            var dbPath = Path.Combine(libraryPath, "dbInventory.db3");

            return new SQLiteConnection(new SQLitePlatformIOS(), dbPath);
        }
    }
}
