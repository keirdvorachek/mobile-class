﻿using Xamarin.Forms;
using Inventory.Custom;
using System.Collections.Generic;
using Inventory.Models;

namespace Inventory
{
    public partial class App : Application
    {
        static Database _inventoryDB;

        public static Database SQLiteDB
        {
            get
            {
                if (_inventoryDB == null)
                {
                    _inventoryDB = new Database();
                }
                return _inventoryDB;
            }
        }

        public App()
        {
            InitializeComponent();

            MainPage = new TabController();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
