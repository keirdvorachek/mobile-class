﻿using System;
using Inventory.Views;
using Xamarin.Forms;

namespace Inventory
{
    public class TabController : TabbedPage
    {
        public TabController()
        {
            Children.Add(new ItemAdd() { Title = "Add"  }); 
            Children.Add(new ItemCount() { Title = "Count"   });
            Children.Add(new ItemOrder() { Title = "Order"   });
        }
    }//Icon = "movietab"
}

