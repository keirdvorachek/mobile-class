﻿using System;
using System.Collections.Generic;
using Inventory.Models;

using Xamarin.Forms;

namespace Inventory.Views
{
    public partial class ItemAdd : ContentPage
    {
        public ItemAdd()
        {
            InitializeComponent();
        }

        void AddItem_Clicked(object sender, System.EventArgs e)
        {
            var NewItem = new AddModel();
            NewItem.ItemName = txtItemName.Text;
            NewItem.ItemSKU = txtItemSKU.Text;
            NewItem.ItemPAR = txtItemPar.Text;

            App.SQLiteDB.SaveItem(NewItem);

            txtItemName.Text = string.Empty;
            txtItemSKU.Text = string.Empty;
            txtItemPar.Text = string.Empty;

        }
    }
}
