﻿using System;
using SQLite.Net.Attributes;

namespace Inventory.Models
{
    public class AddModel
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string ItemName { get; set; }
        public string ItemSKU { get; set; }
        public string ItemPAR { get; set; }
    }
}
