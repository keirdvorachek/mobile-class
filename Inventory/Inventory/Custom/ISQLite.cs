﻿using System;
using SQLite.Net;

namespace Inventory.Custom
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
