﻿using System;
using System.Collections.Generic;
using Inventory.Models;
using SQLite.Net;
using Xamarin.Forms;

namespace Inventory.Custom
{
    public class Database
    {
        
        SQLiteConnection database;
        static object locker = new object();

        public Database()
        {
            //Get Connection String
            database = DependencyService.Get<ISQLite>().GetConnection();

            //Create Database
            database.CreateTable<AddModel>();
        }

        public void SaveItem(AddModel item)
        {
            lock (locker)
            {
                database.Insert(item);
            }

        }

        public List<AddModel> GetItems()
        {
            var Result = database.Query<AddModel>("SELECT * FROM [AddModel]");

            return Result;
        }
    }

}
