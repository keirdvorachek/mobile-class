﻿using System;
using Android.Graphics;
using RoundedCorners.Custom;
using RoundedCorners.Droid.Custom;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ImageBox), typeof(ImageBoxRenderer))]


namespace RoundedCorners.Droid.Custom
{
    public class ImageBoxRenderer: ImageRenderer
    {

        public ImageBoxRenderer()
        {
            // This Constructor I know is redundant and useless.
            // However it allows this nice orange line to only surround it,
            // instead of this entire page for reasons I do not have the knowledge to yet.
        }

        protected override bool DrawChild(Android.Graphics.Canvas canvas, Android.Views.View child, long drawingTime)
        {
            //Clip path
            var radius = 25.0f;
            var path = new Path();
            RectF rect = new RectF(0, 0, Width, Height);
            path.AddRoundRect(rect, radius,radius,Path.Direction.Cw);
            canvas.Save();
            canvas.ClipPath(path);
            var result = base.DrawChild(canvas, child, drawingTime);
            canvas.Restore();

            //Create Boarder
            path = new Path();
            path.AddRoundRect(rect, radius, radius, Path.Direction.Cw);
            var paint = new Paint();
            paint.AntiAlias = true;
            paint.StrokeWidth = 1;
            paint.SetStyle(Paint.Style.Stroke);
            paint.Color = global::Android.Graphics.Color.Rgb(11, 11, 14);
            canvas.DrawPath(path,paint);

            //Clean Up
            paint.Dispose();
            path.Dispose();

            return result;


        }
        //return base.DrawChild(canvas, child, drawingTime);
        //      Wanted to show that I followed along with this portion of the video, 
        //      however I believe with using Visual Studio and with a newer version this code is not needed.
    }
}
