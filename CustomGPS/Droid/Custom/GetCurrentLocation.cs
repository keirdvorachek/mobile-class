﻿using System;
using CustomGPS.Custom;
using Xamarin.Forms;
using Plugin.Geolocator;
using System.Threading.Tasks;

[assembly: Dependency(typeof(CustomGPS.Droid.Custom.GetCurrentLocation))]

namespace CustomGPS.Droid.Custom
{
    public class GetCurrentLocation : ICurrentLocation
    {
        public event EventHandler LocationUpdated;

        public GetCurrentLocation()
        {

        }
        public void UpdateCurrentLocation()
        {
            //Get GPS Location
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 100;

            locator.GetPositionAsync(TimeSpan.FromSeconds(10)).ContinueWith(t =>
            {
                App.CurrentLon = t.Result.Longitude;
                App.CurrentLat = t.Result.Latitude;

                //Flag process as done
                LocationUpdated(null, null);

            }, TaskScheduler.FromCurrentSynchronizationContext());


        }
    }
}
