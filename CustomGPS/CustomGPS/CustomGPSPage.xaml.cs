﻿using CustomGPS.Custom;
using Xamarin.Forms;

namespace CustomGPS
{
    public partial class CustomGPSPage : ContentPage
    {
        public CustomGPSPage()
        {
            InitializeComponent();
            DependencyService.Get<ICurrentLocation>().LocationUpdated += delegate 
            {
                lblLon.Text = "LON: " + App.CurrentLon.ToString();
                lblLat.Text = "LAT: " + App.CurrentLat.ToString();       
            };
        }

        void UpdateLocation_Clicked(object sender, System.EventArgs e)
        {
            DependencyService.Get<ICurrentLocation>().UpdateCurrentLocation();
        }

        void Clear_Clicked(object sender, System.EventArgs e)
        {
            lblLon.Text = "LON: 0000000";
            lblLat.Text = "LAT: 0000000";
        }
    }
}
